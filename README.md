# WH - Risk Management

This project is a simple risk application.

## GIT setup

You can clone using ssh with a preshared SSH key with the following command:

    git clone https://leahrosacena@bitbucket.org/leahrosacena/wh.riskmngt.git

## Setup
You will need 'NodeJS' installed. After you install you will need [Webpack](https://webpack.js.org/). To install run the following:

    npm install -g webpack

Then you will need to run the below commands in the 'WH.RiskMngt.Web' folder:

    npm install
    npm run build

## Running
Open solution 'WH.RiskMngt' in Visual Studio 2017. Right click on the solution and click 'Set Startup Projects'. Select 'Multiple startup projects'. In the list of projects set action to 'Start' for WH.RiskMngt.Api and WH.RiskMngt.Web then hit F5. It should launch 2 broswers.

    * Web API
       http://localhost:22206/
    * Web
       http://localhost:22776/