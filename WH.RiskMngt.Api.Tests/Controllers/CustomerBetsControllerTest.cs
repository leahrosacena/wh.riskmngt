﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WH.RiskMngt.Api;
using WH.RiskMngt.Api.Controllers;
using Moq;
using WH.RiskMngt.Business;
using System.Threading.Tasks;
using WH.RiskMngt.Model.Entities;
using System.Web.Http.Results;

namespace WH.RiskMngt.Api.Tests.Controllers
{
    [TestClass]
    public class CustomerBetsControllerTest
    {
        private Mock<ICustomerBetManager> _customerBetManager;

        [TestInitialize]
        public void Initialize()
        {
            _customerBetManager = new Mock<ICustomerBetManager>();
        }

        [TestMethod]
        public async Task ShouldGetCustomerBetsHistory()
        {
            //Arrange
            Initialize();
            var mockResults = Task.FromResult(new List<CustomerBetHistory>
            {
                new CustomerBetHistory
                {
                    Customer = 1,
                    CustomerSettledBets = new List<CustomerSettledBet>
                    {
                        new CustomerSettledBet
                        {
                            Customer = 1, Event = 1, Participant = 6, Stake = 50, Win = 250
                        }
                    },
                    CustomerUnsettledBets = new List<CustomerUnsettledBet>
                    {
                        new CustomerUnsettledBet
                        {
                            Customer = 1, Event = 11, Participant = 4, Stake = 50, ToWin = 500
                        }
                    }
                }
            });

            _customerBetManager.Setup(a => a.GetCustomerBetHistory()).Returns(mockResults);

            var controller = new CustomerBetsController(_customerBetManager.Object);
            //Act
            var result = await controller.GetHistory() as OkNegotiatedContentResult<List<CustomerBetHistory>>;

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Content.Count());
            Assert.AreEqual(1, result.Content[0].Customer);
            Assert.AreEqual(1, result.Content[0].CustomerSettledBets.Count());
            Assert.AreEqual(1, result.Content[0].CustomerUnsettledBets.Count());
        }

        [TestMethod]
        public async Task ShouldThrowErrorWhenCustomerBetHistoryIsNull()
        {
            // Arrange
            _customerBetManager.Setup(a => a.GetCustomerBetHistory()).Returns(Task.FromResult<List<CustomerBetHistory>>(null));

            var controller = new CustomerBetsController(_customerBetManager.Object);

            // Act
            var result = await controller.GetHistory() as BadRequestErrorMessageResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("No data found.", result.Message);
        }        
    }
}
