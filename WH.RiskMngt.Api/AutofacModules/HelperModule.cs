﻿using Autofac;
using System.Linq;

namespace WH.RiskMngt.Api.AutofacModules
{
    public class HelperModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(System.Reflection.Assembly.Load("WH.RiskMngt.Common"))
                   .Where(t => t.Name.EndsWith("Helper"))
                   .PropertiesAutowired()
                   .AsSelf()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}