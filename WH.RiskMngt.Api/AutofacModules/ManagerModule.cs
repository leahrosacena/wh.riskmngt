﻿using Autofac;
using System.Linq;

namespace WH.RiskMngt.Api.AutofacModules
{
    public class ManagerModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            builder.RegisterAssemblyTypes(System.Reflection.Assembly.Load("WH.RiskMngt.Business"))
                   .Where(t => t.Name.EndsWith("Manager"))
                   .PropertiesAutowired()
                   .AsSelf()
                   .AsImplementedInterfaces()
                   .InstancePerLifetimeScope();
        }
    }
}