﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WH.RiskMngt.Business;

namespace WH.RiskMngt.Api.Controllers
{
    [RoutePrefix("api/customerbets")]
    public class CustomerBetsController : ApiController
    {
        public readonly ICustomerBetManager _customerBetManager;

        public CustomerBetsController(ICustomerBetManager customerBetManager)
        {
            _customerBetManager = customerBetManager;
        }

        [Route("history"), HttpGet]
        public async Task<IHttpActionResult> GetHistory()
        {
            try
            {
                var customerBetHistory = await _customerBetManager.GetCustomerBetHistory();
                if (customerBetHistory == null)
                {
                    return BadRequest("No data found.");
                }
                return Ok(customerBetHistory);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
