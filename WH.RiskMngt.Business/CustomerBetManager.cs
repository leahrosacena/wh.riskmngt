﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WH.RiskMngt.Common.Helpers;
using WH.RiskMngt.Model.Entities;

namespace WH.RiskMngt.Business
{
    public class CustomerBetManager : ICustomerBetManager
    {
        public IHttpClientHelper _httpClientHelper { get; set; }
        public IConfigurationHelper _configurationHelper { get; set; }
        public string WhCustomerBetApi
        {
            get
            {
                return _configurationHelper.GetAppSettings("WhCustomerBetApi");
            }
        }

        public CustomerBetManager(IHttpClientHelper httpClientHelper, IConfigurationHelper configurationHelper)
        {
            _httpClientHelper = httpClientHelper;
            _configurationHelper = configurationHelper;
        }

        public async Task<List<CustomerBetHistory>> GetCustomerBetHistory()
        {
            var settledBets = await GetCustomerSettledBetHistory();
            var unsettledBets = await GetCustomerUnsettledBetHistory();

            var settledCustomerBets = settledBets.GroupBy(a => a.Customer)
                .Select(grp => new CustomerBetHistory
                {
                    Customer = grp.Key,
                    CustomerSettledBets = grp.ToList()
                }).ToList();

            var unsettledCustomerBets = unsettledBets.GroupBy(a => a.Customer)
                .Select(grp => new CustomerBetHistory
                {
                    Customer = grp.Key,
                    CustomerUnsettledBets = grp.ToList()
                }).ToList();

            var customerBetHistoryMerged = settledCustomerBets.Select(sc => {
                sc.CustomerUnsettledBets = unsettledBets.Where(uc => uc.Customer == sc.Customer).ToList();
                return sc;
            });
            
            return customerBetHistoryMerged.ToList();
        }

        public async Task<List<CustomerSettledBet>> GetCustomerSettledBetHistory()
        {
            var settledBets = await _httpClientHelper.Get<List<CustomerSettledBet>>(WhCustomerBetApi, "settled");
            return settledBets;
        }

        public async Task<List<CustomerUnsettledBet>> GetCustomerUnsettledBetHistory()
        {
            var unsettledBets = await _httpClientHelper.Get<List<CustomerUnsettledBet>>(WhCustomerBetApi, "unsettled");
            return unsettledBets;
        }        
    }
}
