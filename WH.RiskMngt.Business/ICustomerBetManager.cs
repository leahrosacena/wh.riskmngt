﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WH.RiskMngt.Model.Entities;

namespace WH.RiskMngt.Business
{
    public interface ICustomerBetManager
    {
        Task<List<CustomerBetHistory>> GetCustomerBetHistory();
        Task<List<CustomerSettledBet>> GetCustomerSettledBetHistory();
        Task<List<CustomerUnsettledBet>> GetCustomerUnsettledBetHistory();
    }
}
