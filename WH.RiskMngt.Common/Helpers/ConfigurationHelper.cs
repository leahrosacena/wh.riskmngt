﻿using System.Configuration;

namespace WH.RiskMngt.Common.Helpers
{
    public class ConfigurationHelper : IConfigurationHelper
    {
        public string GetAppSettings(string key)
        {
            var setting = ConfigurationManager.AppSettings.Get(key);
            return setting;
        }
    }
}
