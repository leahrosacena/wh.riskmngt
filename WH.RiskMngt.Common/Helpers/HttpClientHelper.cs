﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WH.RiskMngt.Common.Helpers
{
    public class HttpClientHelper : IHttpClientHelper, IDisposable
    {
        public string BaseUri { get; set; }

        private bool _disposed;
        private IConfigurationHelper _configurationHelper;
        public IConfigurationHelper ConfigurationHelper
        {
            get { return _configurationHelper ?? new ConfigurationHelper(); }
            set { _configurationHelper = value; }
        }

        public int Timeout
        {
            get
            {
                var configVal = ConfigurationHelper.GetAppSettings("HttpClientTimeout");
                if (string.IsNullOrEmpty(configVal))
                {
                    return 120;
                }

                int timeout;
                return !int.TryParse(configVal, out timeout) ? 120 : timeout;
            }
        }

        private HttpClient _httpClient;
        public HttpClient HttpClient
        {
            get
            {
                if (_httpClient != null && _httpClient.BaseAddress.AbsoluteUri == BaseUri)
                {
                    //SetHttpClientProperties();
                    return _httpClient;
                }

                if (string.IsNullOrEmpty(BaseUri))
                {
                    BaseUri = ConfigurationHelper.GetAppSettings("SiteUrl");
                }

                _httpClient = new HttpClient
                {
                    BaseAddress = new Uri(BaseUri),
                    Timeout = new TimeSpan(0, 0, 0, Timeout)
                };

                //SetHttpClientProperties();
                return _httpClient;
            }
            set { _httpClient = value; }
        }

        public async Task<TResult> Get<TResult>(string baseUri, string url) where TResult : class
        {
            try
            {
                BaseUri = baseUri;

                var result = await HttpClient.GetAsync(url).Result.Content.ReadAsStringAsync();
                return DeserializeJson<TResult>(result);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
            {
                if (HttpClient != null)
                {
                    HttpClient.Dispose();
                }
            }

            _disposed = true;
        }

        private static T DeserializeJson<T>(string json)
        {
            try
            {
                var obj = JsonConvert.DeserializeObject<T>(json);
                return obj;
            }
            catch (Exception)
            {
                return default(T);
            }
        }

    }
}
