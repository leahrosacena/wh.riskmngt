﻿namespace WH.RiskMngt.Common.Helpers
{
    public interface IConfigurationHelper
    {
        string GetAppSettings(string key);
    }
}
