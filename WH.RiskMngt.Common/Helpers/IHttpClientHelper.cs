﻿using System.Threading.Tasks;

namespace WH.RiskMngt.Common.Helpers
{
    public interface IHttpClientHelper
    {
        Task<TResult> Get<TResult>(string baseUri, string url)
            where TResult : class;
    }
}
