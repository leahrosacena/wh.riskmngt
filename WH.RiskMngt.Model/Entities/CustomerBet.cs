﻿using Newtonsoft.Json;

namespace WH.RiskMngt.Model.Entities
{
    public class CustomerBet
    {
        [JsonProperty(PropertyName = "customer")]
        public int Customer { get; set; }

        [JsonProperty(PropertyName = "event")]
        public int Event { get; set; }

        [JsonProperty(PropertyName = "participant")]
        public int Participant { get; set; }

        [JsonProperty(PropertyName = "stake")]
        public decimal Stake { get; set; }
    }
}
