﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WH.RiskMngt.Model.Entities
{
    public class CustomerBetHistory
    {
        [JsonProperty(PropertyName = "customer")]
        public int Customer { get; set; }
        [JsonProperty(PropertyName = "settledBets")]
        public List<CustomerSettledBet> CustomerSettledBets { get; set; }
        [JsonProperty(PropertyName = "unsettledBets")]
        public List<CustomerUnsettledBet> CustomerUnsettledBets { get; set; }
    }
}
