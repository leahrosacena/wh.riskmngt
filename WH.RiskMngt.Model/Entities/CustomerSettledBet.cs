﻿using Newtonsoft.Json;

namespace WH.RiskMngt.Model.Entities
{
    public class CustomerSettledBet : CustomerBet
    {
        [JsonProperty(PropertyName = "win")]
        public decimal Win { get; set; }
    }
}
