﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WH.RiskMngt.Model.Entities
{
    public class CustomerUnsettledBet : CustomerBet
    {
        [JsonProperty(PropertyName = "toWin")]
        public decimal ToWin { get; set; }
    }
}
