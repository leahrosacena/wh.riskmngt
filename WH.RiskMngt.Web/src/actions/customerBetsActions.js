﻿import * as types from '../constants/actionTypes';
import customerBetsApi from '../api/customerBetsApi';

export function loadCustomerBetsHistorySuccess(customerBets) {
    return { 
        type: types.LOAD_CUSTOMER_BET_HISTORY_SUCCESS, 
        customerBets: customerBets };
}

export function loadCustomerBetsHistory() {
    return function (dispatch) {
        return customerBetsApi.getHistory().then(customerBetsHistory => {
            dispatch(loadCustomerBetsHistorySuccess(customerBetsHistory));
        }).catch(error=> {
            throw(error);
        });
    };
}