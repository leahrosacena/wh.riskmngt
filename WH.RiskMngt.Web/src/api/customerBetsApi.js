import Axios from 'axios';
import _ from 'lodash';
import riskLevel from '../constants/riskLevel';
import delay from './delay';

const customersBetsApi = "http://localhost:22206/api/customerbets";

class CustomerBetsApi {
    static getHistory() {
        return new Promise((resolve, reject) => {
            let url = customersBetsApi + "/history";
            setTimeout(() => {
                Axios.get(url)
                    .then(response => {
                        let customerBets = this.updateRiskLevel(response.data);
                        resolve(Object.assign([], customerBets));
                    })
                    .catch(error => {
                        throw(error);
                    });        
            }, delay);
        });
    }
    
    static updateRiskLevel(customerBetsHistory){
        let customerBets = [];      
        for (let item of customerBetsHistory) {
            let numberOfWins = _.filter(item.settledBets, function(bet){
                return bet.win > 0;
            }).length;
            
            let isBetsRisky = this.isBetsHighRisk(numberOfWins, item.settledBets.length);
            let averageStake = this.getAverageStake(item);
            for (var unsettledBet of item.unsettledBets) {
                if(isBetsRisky || unsettledBet.stake >= (averageStake * 10) || unsettledBet.toWin >= 1000)
                {
                    unsettledBet.riskLevel = riskLevel.Moderate;
                }

                if(unsettledBet.stake >= (averageStake * 30)){
                    unsettledBet.riskLevel = riskLevel.High;
                }
                if(unsettledBet.riskLevel === null || unsettledBet.riskLevel === undefined){
                    unsettledBet.riskLevel = riskLevel.Low;
                }
            }
            item.riskLevel = isBetsRisky ? riskLevel.Moderate : riskLevel.Low;
            customerBets.push(item);
        }
        return customerBets;
    }

    static isBetsHighRisk(numberOfWins, totalNumberOfBets){
        let percentage = (numberOfWins/totalNumberOfBets) * 100;
        return percentage >= 60;
    }

    static getAverageStake(customerBet){        
        let totalSettledStake = _.sumBy(customerBet.settledBets, function(bet){
            return bet.stake;
        });
        let totalUnsettledStake = _.sumBy(customerBet.unsettledBets, function(bet){
            return bet.stake;
        });
        let totalStake = totalSettledStake + totalUnsettledStake;
        return (totalStake/ (customerBet.settledBets.length + customerBet.unsettledBets.length));
    }
}
export default CustomerBetsApi;