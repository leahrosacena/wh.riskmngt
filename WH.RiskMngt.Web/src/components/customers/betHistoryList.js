import React, { Component, PropTypes } from 'react';
import BetHistoryListRow from './betHistoryListRow';

const BetHistoryList = ({bets, isSettled}) => {
    return (
        <table className="table table-bordered">
            <thead className="thead-inverse">
                <tr>
                    <th>Event</th>
                    <th>Participant</th>
                    <th>Stake</th>
                    <th>{isSettled ? 'Win' : 'To win'}</th>
                    <th className={isSettled? 'hidden':''}>Risk</th>
                </tr>
            </thead>
            <tbody>
                { bets.map((bet, index) => <BetHistoryListRow key={index} bet={bet} isSettled={isSettled}/>) }
            </tbody>
        </table>
    );
};

BetHistoryList.propTypes = {
    bets: PropTypes.array.isRequired,
    isSettled: PropTypes.bool.isRequired
}

export default BetHistoryList;