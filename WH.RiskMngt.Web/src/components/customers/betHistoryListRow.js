import React, { Component, PropTypes } from 'react';
import { FormattedNumber } from 'react-intl';
import riskLevel from '../../constants/riskLevel';

const BetHistoryListRow = ({bet, isSettled}) => {
    return (
        <tr className={'alert ' + (bet.riskLevel === riskLevel.Moderate ? 'alert-warning' : (bet.riskLevel === riskLevel.High ? 'alert-danger' : ''))}>
            <td>{bet.event}</td>
            <td>{bet.participant}</td>
            <td>
                <FormattedNumber style="decimal" maximumFractionDigits={2} minimumFractionDigits={2} value={bet.stake} />
            </td>
            <td>
                <FormattedNumber style="decimal" maximumFractionDigits={2} minimumFractionDigits={2} value={isSettled ? bet.win : bet.toWin} />
            </td>
            <td className={isSettled? 'hidden':''}>
                {
                    bet.riskLevel === riskLevel.Moderate || bet.riskLevel === riskLevel.High ?
                    <span>
                        {bet.riskLevel === riskLevel.Moderate ? "Moderate" : (bet.riskLevel === riskLevel.High ? "High" : "") }
                    </span> :
                    <span></span>
                }
            </td>
        </tr>
    );
};

BetHistoryListRow.propTypes = {
    bet: PropTypes.object.isRequired,
    isSettled: PropTypes.bool.isRequired
}

export default BetHistoryListRow;