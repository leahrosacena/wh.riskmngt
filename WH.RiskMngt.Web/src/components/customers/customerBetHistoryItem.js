import React, { Component, PropTypes } from 'react';
import {Icon} from 'react-fa';

import riskLevel from '../../constants/riskLevel';
import BetHistoryList from './betHistoryList';

const CustomerBetHistoryItem = ({ customerBetHistoryItem }) => (
    <div className="container">
        <h2>
            {
                customerBetHistoryItem.riskLevel === riskLevel.Moderate ?
                <span className="lable label-danger">High Risk</span> : <span> </span>
            }
            <span>{' Customer ' + customerBetHistoryItem.customer }</span>
        </h2>
        <div className="container">
            <h3>Settled Bets</h3>
            <BetHistoryList bets={customerBetHistoryItem.settledBets} isSettled={true}></BetHistoryList>
        </div>
        <div className="container">
            <h3>Unsettled Bets</h3>
            <BetHistoryList bets={customerBetHistoryItem.unsettledBets} isSettled={false}></BetHistoryList>
        </div>
    </div>
);

CustomerBetHistoryItem.propTypes = {
    customerBetHistoryItem: PropTypes.object.isRequired
}

export default CustomerBetHistoryItem;