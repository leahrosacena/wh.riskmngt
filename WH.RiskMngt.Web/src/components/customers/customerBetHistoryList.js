import React, { Component, PropTypes } from 'react'
import CustomerBetHistoryItem from './customerBetHistoryItem';
import {Icon} from 'react-fa';


const CustomerBetHistoryList = ({customerBets}) => {
    return (
        <div className="customer-bet-history">
            <div className="jumbotron">
                {
                    (!customerBets || customerBets.length == 0) ?
                    <div className="alert alert-info">   
                        <Icon name="info-circle" size="2x" className="pull-left"/>               
                        <h4>No bets available.</h4>
                    </div>
                    :
                    customerBets.map((item, index) => <CustomerBetHistoryItem key={index} customerBetHistoryItem={item}></CustomerBetHistoryItem>)
                }
            </div>
        </div>
    );
};

CustomerBetHistoryList.propTypes = {
    customerBets: PropTypes.array.isRequired
}

export default CustomerBetHistoryList;