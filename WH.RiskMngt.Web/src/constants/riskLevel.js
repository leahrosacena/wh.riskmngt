const RiskLevel = {
    Low: "low",
    Moderate: "moderate",
    High: "high"
};

export default RiskLevel;