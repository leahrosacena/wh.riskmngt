import React, { Component, PropTypes } from 'react'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as customerBetsActions from '../../actions/customerBetsActions';
import CustomerBetHistoryList from '../../components/customers/customerBetHistoryList';

class CustomersPage extends Component {
    constructor(props){
        super(props);

    }

    componentDidMount() {
        this.props.getCustomerBets();
        this.setState({
            isLoaded: true,
        });
    }

    render () {
        return (
            <div className="customers">  
                <CustomerBetHistoryList customerBets={this.props.customerBets}></CustomerBetHistoryList>              
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    let customerBets = state.customerBets ? state.customerBets : [];

    return {
        customerBets: customerBets
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCustomerBets: () => {
            dispatch(customerBetsActions.loadCustomerBetsHistory());
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomersPage);
