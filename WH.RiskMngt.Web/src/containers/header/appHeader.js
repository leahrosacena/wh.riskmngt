﻿import React, { Component, PropTypes } from 'react';
import { IndexLink, Link } from 'react-router';

const Header = ({}) => {
    return (
        <nav>
            <IndexLink to="/" activeClassName="active"><h3>Risk Management</h3></IndexLink>
        </nav>
    );
};

export default Header;