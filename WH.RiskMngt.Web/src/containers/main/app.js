﻿import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import AppHeader from '../header/appHeader';
import CustomersPage from '../customers/customersPage';

class App extends Component {
    render() {
        return (
            <div className="container-fluid">
                <AppHeader />
                <CustomersPage />
            </div>
        );
    }
}

export default App;