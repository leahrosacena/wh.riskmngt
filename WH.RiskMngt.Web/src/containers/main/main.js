﻿import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Bootstrap from 'bootstrap/dist/css/bootstrap.css';
import { Router, Route, IndexRoute, Link, browserHistory } from 'react-router';
import { IntlProvider } from 'react-intl';
import {loadCustomerBetsHistory} from '../../actions/customerBetsActions';
import configureStore from '../../stores/configureStore';

import App from './App';

const store = configureStore();

ReactDOM.render((
    <Provider store={store}>
        <IntlProvider locale="en">
            <App />
        </IntlProvider>
    </Provider>
), document.getElementById('app'));