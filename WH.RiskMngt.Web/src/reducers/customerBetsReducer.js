﻿import * as types from '../constants/actionTypes';
import initialState from './initialState';

export default function customerBetsReducer(state = initialState.customerBets, action) {
    switch (action.type) {
        case types.LOAD_CUSTOMER_BET_HISTORY_SUCCESS:
            return action.customerBets;
        default:
            return state;
    } 
}