import {combineReducers} from 'redux';
import { routerReducer } from 'react-router-redux';
import customerBets from './customerBetsReducer';

const rootReducer = combineReducers({
    customerBets,
	routerReducer: routerReducer
});

export default rootReducer;